/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     24/08/2018
% Version:  V1
%
% Compute the geometry for electromagnetic energy harvester (SWS-Microsys) : 
%       Input  : -
%       output : Complete geometry used in GMSH
****************************************************************************/

SetFactory("OpenCASCADE");

// Units
mum   = 1.e-6;
mm    = 1.e-3;
cm    = 1.e-2;
dm    = 1.e-1;

VolMagnets[] = {}; // for 3D model
VolMagn[] = {};

SurfMagnets2D[] = {}; // for 2D model

// Include all geometrical parts
Include "Parameters/Electromagnetic_EH_common.pro";  // User interface parameters value
Include "Geometry/magnets.geo";                      // Permanent magnet
Include "Geometry/cores.geo";                        // Ferromagnetic core
If(Spacers == 1)
  Include "Geometry/spacers.geo";                    // Ferromagnetic spacers (between magnets)
EndIf
Include "Geometry/windings.geo";                     // Copper coil
Include "Geometry/InfiniteBox.geo";                  // Domain and infinit domain

// set some global Gmsh options
Mesh.Optimize = 6;              // optimize quality of tetrahedra
Mesh.VolumeEdges = 1;           // Toggle mesh display
Mesh.SurfaceEdges = 0; 
Solver.AutoMesh = 2;            // always remesh if necessary (don't reuse mesh on disk)

// The overall dimensions of the model have been calculated in InfiniteBox.geo
// So we use to characteristic length set for the infinite box for the whole mesh.
//Mesh.CharacteristicLengthMin = lc1inf;
Mesh.CharacteristicLengthMax = lc1inf;


// Size of the external domain while knowing the size of each parts (magnets, cores, spacers,...)
If(!Flag_3Dto2D)

  AirBox[] = BooleanDifference{ Volume{ InteriorInfBox }; Delete; }{ Volume{ VolMagnets[]};};
  Physical Volume("AirBox",4) = { AirBox[] };

  Volumes[] = { VolInf[], VolMagnets[], AirBox[] };

  vv[] = BooleanFragments{
    Volume { Volumes[] }; Delete; }{};

  If( #vv[] > #Volumes[] )
    Error("Overlapping Structures");
    Abort;
  EndIf

  Printf("Check whether BooleanFragments has preserved volume numbering:");
  For num In {0:#vv()-1}
    Printf("Fragment %5g -> %g", Volumes[num], vv(num));
  EndFor

  Outer[] = CombinedBoundary{ Volume{ Volumes[] }; };
  Physical Surface("OuterSurface", 5) = { Outer[]  };
Else
  AirBox[] = BooleanDifference{Surface{Label_InnerBox}; Delete;}{ Surface{SurfMagnets2D[]}; };
  Physical Surface("AirBox",4) = { AirBox[] };

  Surfaces[] = { SurInf[], SurfMagnets2D[], AirBox[] };

  vv[] = BooleanFragments{
    Surface { Surfaces[] }; Delete; }{};

  If( #vv[] > #Surfaces[] )
    Error("Overlapping Structures");
    Abort;
  EndIf

  Printf("Check whether BooleanFragments has preserved volume numbering:");
  For num In {0:#vv()-1}
    Printf("Fragment %5g -> %g", Surfaces[num], vv(num));
  EndFor

  Outer[] = CombinedBoundary{ Surface{ Surfaces[] }; };
  Physical Line("OuterSurface", 5) = { Outer[]  };


EndIf

