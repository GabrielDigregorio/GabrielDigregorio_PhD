/****************************************************************************
% Name:     Gabriel Digregorio
% Date:     24/08/2018
% Version:  V1
%
% Chose between 2D or 3D .pro file : 
%       Input  : Variable "Flag_3Dto2D" in _common.pro
%       output : Open the 2D or 3D .pro file
*****************************************************************************/

Include "Parameters/Flag3Dto2D.pro";

If(Flag_3Dto2D==1) // 2D
    Include "Physics/Electromagnetic_EH_2D.pro";
Else // 3D
    Include "Physics/Electromagnetic_EH_3D.pro";
EndIf


