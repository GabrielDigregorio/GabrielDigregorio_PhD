/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     22/08/2018
% Version:  V1
%
% Compute the geometry for the ferromagnetic core : 
%       Input  : Electromagnetic_EH_common.pro values of variables
%       output : Geometry for the ferromagnetic core
****************************************************************************/

SetFactory("OpenCASCADE");

mm = 1.e-3;

For j In {1:NumCores}
    jLabel = newv;
    If(M_core~{j} == 0) // cylinder type 1
        ExternalCyl1 = newv;
        Cylinder(ExternalCyl1) = { X_core~{j}, (Y_core~{j}+(L_core~{j})/2), Z_core~{j}, 0, ((L_core~{j}-l_core~{j})/2), 0, R_core~{j} };
        ExternalCyl2 = newv;
        Cylinder(ExternalCyl2) = { X_core~{j}, (Y_core~{j}-(L_core~{j})/2), Z_core~{j}, 0, ((L_core~{j}-l_core~{j})/2), 0, R_core~{j} };
        InternalCyl1 = newv;
        Cylinder(InternalCyl1) = { X_core~{j}, Y_core~{j}-((L_core~{j}-l_core~{j})/2), Z_core~{j}, 0, L_core~{j}-((L_core~{j}-l_core~{j})/2), 0, r_core~{j} };
        jLabel = newv;
        BooleanUnion(jLabel) =  {Volume{ExternalCyl1}; Volume{ExternalCyl2}; Delete;} {Volume{InternalCyl1}; Delete;};
    ElseIf(M_core~{j} == 1) // SWS Microsys geometry
        jLabel() = ShapeFromFile("SWS_Core_Geometry/SWS_Solidworks_Noyau_AWE_F126.STEP");
        //Geometry.OCCTargetUnit = "M";
        Rotate { {0,0,1}, {X_core~{j},0,Z_core~{j}}, deg*(90) } { Volume{ jLabel }; }
        Translate {0,Y_core~{j}+0.0005,0} { Volume{ jLabel }; }
    EndIf

    If(M_core~{j} == 0) // cylinder type 1
        Rotate { {0,0,1}, {X_core~{j},0,Z_core~{j}}, deg*Rz_core~{j} } { Volume{ jLabel }; }
        Rotate { {0,1,0}, {X_core~{j},0,Z_core~{j}}, deg*Ry_core~{j} } { Volume{ jLabel }; }
        Rotate { {1,0,0}, {X_core~{j},0,Z_core~{j}}, deg*Rx_core~{j} } { Volume{ jLabel }; }
    ElseIf(M_core~{j} == 1) // SWS Microsys geometry
        Rotate { {0,0,1}, {X_core~{j},Y_core~{j},Z_core~{j}}, deg*Rz_core~{j} } { Volume{ jLabel }; }
        Rotate { {0,1,0}, {X_core~{j},Y_core~{j},Z_core~{j}}, deg*(Ry_core~{j}+90) } { Volume{ jLabel }; }
        Rotate { {1,0,0}, {X_core~{j},Y_core~{j},Z_core~{j}}, deg*Rx_core~{j} } { Volume{ jLabel }; }
        Translate {0,1.4*L_core~{j},0} { Volume{ jLabel }; }
    EndIf

    If(!Flag_3Dto2D)
            Physical Volume(Sprintf("Magnet_%g",j+NumMagnets),10*(j+NumMagnets)) = { jLabel };
            skin~{j+NumMagnets}[] = CombinedBoundary{ Volume{ jLabel }; };
            Physical Surface(Sprintf("SkinMagnet_%g",j+NumMagnets),10*(j+NumMagnets)+1) = -skin~{j+NumMagnets}[]; // Core skin
            VolMagnets[] +=  { jLabel };

            // Mesh
            Characteristic Length { PointsOf{ Volume{jLabel}; } } = MeshCharacteristicLength_core;

        Else
            RectangleCut = newv;
            Rectangle(RectangleCut) = {0.0, -0.05, 0, 0.1, 0.1, 0};
            jLabel_2D = newv;
            BooleanIntersection(jLabel_2D) = { Volume{ jLabel }; Delete; } { Surface{RectangleCut}; Delete; };
            Physical Surface(Sprintf("2DCore_%g",10*(j+NumMagnets)),10*(j+NumMagnets)) = { jLabel_2D };
            Physical Line(Sprintf("2DCore_%g",10*(j+NumMagnets)+1),10*(j+NumMagnets)+1) = Boundary{Surface{jLabel_2D};};
            SurfMagnets2D[] +=  { jLabel_2D };

            // Mesh
            Characteristic Length { PointsOf{ Surface{jLabel_2D}; } } = MeshCharacteristicLength_core;
    EndIf
    
    
EndFor