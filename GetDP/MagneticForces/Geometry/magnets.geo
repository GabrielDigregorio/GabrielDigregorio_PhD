/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     14/08/2018
% Version:  V1
%
% Compute the geometry for the permanent magnets : 
%       Input  : Electromagnetic_EH_common.pro values of variables
%       output : Geometry for the permanent magnets
****************************************************************************/

SetFactory("OpenCASCADE");

mm = 1.e-3;


For i In {1:NumMagnets}
  iLabel = newv;
  If(M~{i} == 0) // cylinder type 1
    ExternalCyl = newv; 
    Cylinder(ExternalCyl) = { X~{i}, Y~{i}-L~{i}/2, Z~{i}, 0, L~{i}, 0, R~{i} };
    InternalCyl = newv;
    Cylinder(InternalCyl) = { X~{i}, Y~{i}-L~{i}/2, Z~{i}, 0, L~{i}, 0, r~{i} };
    iLabel = newv;
    BooleanDifference(iLabel) =  {Volume{ExternalCyl}; Delete;} {Volume{InternalCyl}; Delete;};

  ElseIf(M~{i} == 1) // cylinder type 2
    // write an other type of geometry
  EndIf

  // Rotation
  Rotate { {0,0,1}, {X~{i},Y~{i},Z~{i}}, deg*Rz~{i} } { Volume{ iLabel }; }
  Rotate { {0,1,0}, {X~{i},Y~{i},Z~{i}}, deg*Ry~{i} } { Volume{ iLabel }; }
  Rotate { {1,0,0}, {X~{i},Y~{i},Z~{i}}, deg*Rx~{i} } { Volume{ iLabel }; }

  // Create physical entities (volume and skin)
  If(!Flag_3Dto2D)
    Physical Volume(Sprintf("Magnet_%g",i),10*i) = { iLabel };
    skin~{i}[] = CombinedBoundary{ Volume{ iLabel }; };
    Physical Surface(Sprintf("SkinMagnet_%g",i),10*i+1) = -skin~{i}[]; // magnet skin
    VolMagnets[] += { iLabel };
    
    // Mesh
    Characteristic Length { PointsOf{ Volume{ iLabel}; } } = MeshCharacteristicLength_magnet;

  Else
      RectangleCut_Left  = newv;
      Rectangle(RectangleCut_Left) = {0, -0.05, 0, 0.1, 0.1, 0};
      iLabel_2D_Left = newv;
      BooleanIntersection(iLabel_2D_Left) = { Volume{ iLabel }; Delete;} { Surface{RectangleCut_Left}; Delete;};
      Physical Surface(Sprintf("2DMagnet_%g_Left",10*(i)),10*(i)) = {iLabel_2D_Left};
      Physical Line(Sprintf("2DMagnet_%g_Left" ,10*(i)+2),10*(i)+2) = Boundary{Surface{iLabel_2D_Left};};
      SurfMagnets2D[] +=  { iLabel_2D_Left };

      // Mesh
      Characteristic Length { PointsOf{ Surface{iLabel_2D_Left}; } } = MeshCharacteristicLength_magnet;
  EndIf

  
EndFor









