/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     14/08/2018
% Version:  V1
%
% Compute the geometry for the ferromagnetic spacers : 
%       Input  : Electromagnetic_EH_common.pro values of variables
%       output : Geometry for the ferromagnetic spacers
****************************************************************************/

SetFactory("OpenCASCADE");

mm = 1.e-3;


For k In {1:NumSpacers}
    kLabel = newv;
    If(M_spacer~{k} == 0) // cylinder type 1
        ExternalCyl = newv;
        Cylinder(ExternalCyl) = { X_spacer~{k}, Y_spacer~{k}-L_spacer~{k}/2, Z_spacer~{k}, 0, L_spacer~{k}, 0, R_spacer~{k} };
        InternalCyl = newv;
        Cylinder(InternalCyl) = { X_spacer~{k}, Y_spacer~{k}-L_spacer~{k}/2, Z_spacer~{k}, 0, L_spacer~{k}, 0, r_spacer~{k} };
        kLabel = newv;
        BooleanDifference(kLabel) =  {Volume{ExternalCyl}; Delete;} {Volume{InternalCyl}; Delete;};
    ElseIf(M_spacer~{k} == 1) // cylinder type 2
        // write an other type of geometry
EndIf

    Rotate { {0,0,1}, {X_spacer~{k},Y_spacer~{k},Z_spacer~{k}}, deg*Rz_spacer~{k} } { Volume{ kLabel }; }
    Rotate { {0,1,0}, {X_spacer~{k},Y_spacer~{k},Z_spacer~{k}}, deg*Ry_spacer~{k} } { Volume{ kLabel }; }
    Rotate { {1,0,0}, {X_spacer~{k},Y_spacer~{k},Z_spacer~{k}}, deg*Rx_spacer~{k} } { Volume{ kLabel }; }

    If(!Flag_3Dto2D)
        Physical Volume(Sprintf("Magnet_%g",k+NumMagnets+NumCores),10*(k+NumMagnets+NumCores)) = { kLabel };
        skin~{k+NumMagnets+NumCores}[] = CombinedBoundary{ Volume{ kLabel }; };
        Physical Surface(Sprintf("SkinMagnet_%g",k+NumMagnets+NumCores),10*(k+NumMagnets+NumCores)+1) = -skin~{k+NumMagnets+NumCores}[]; // Core skin
        VolMagnets[] +=  { kLabel };

        // Mesh
        Characteristic Length { PointsOf{ Volume{ kLabel}; } } = MeshCharacteristicLength_spacer;

    Else
        RectangleCut_Left  = newv;
        Rectangle(RectangleCut_Left) = {0, -0.05, 0, 0.1, 0.1, 0};
        kLabel_2D_Left = newv;
        BooleanIntersection(kLabel_2D_Left) = { Volume{ kLabel }; Delete;} { Surface{RectangleCut_Left}; Delete; };
        Physical Surface(Sprintf("2DSpacer_%g_Left",10*(k+NumMagnets+NumCores)),10*(k+NumMagnets+NumCores)) = {kLabel_2D_Left};
        Physical Line(Sprintf("2DSkinSpacer_%g",10*(k+NumMagnets+NumCores)+2),10*(k+NumMagnets+NumCores)+2) = {Boundary{Surface{kLabel_2D_Left};}};
        SurfMagnets2D[] +=  { kLabel_2D_Left };

        // Mesh
        Characteristic Length { PointsOf{ Surface{kLabel_2D_Left}; } } = MeshCharacteristicLength_spacer;
    EndIf

EndFor