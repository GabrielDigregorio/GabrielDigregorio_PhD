/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     14/08/2018
% Version:  V1
%
% Compute the geometry for the winding coper (wire) on the ferromagnetic core : 
%       Input  : Electromagnetic_EH_common.pro values of variables
%       output : Geometry for the winding around the ferromagnetic core
****************************************************************************/

SetFactory("OpenCASCADE");


mm = 1.e-3;

For l In {1:NumCores}
    lLabel = newv;
    If(M_winding~{l} == 0 && M_core~{l} == 0) // cylinder type 1
        ExternalCyl = newv;
        Cylinder(ExternalCyl) = { X_winding~{l}, Y_winding~{l}-((L_winding~{l}-l_core~{l})/2), Z_winding~{l}, 0, L_winding~{l}-((L_winding~{l}-l_core~{l})/2)-2*G_winding~{l}, 0, R_winding~{l}-G_winding~{l} };
        InternalCyl = newv;
        Cylinder(InternalCyl) = { X_winding~{l}, Y_winding~{l}-((L_winding~{l}-l_core~{l})/2), Z_winding~{l}, 0, L_winding~{l}-((L_winding~{l}-l_core~{l})/2)-2*G_winding~{l}, 0, r_winding~{l}+G_winding~{l} };
        lLabel = newv;
        BooleanDifference(lLabel) =  {Volume{ExternalCyl}; Delete;} {Volume{InternalCyl}; Delete;};
    ElseIf(M_winding~{l} == 1 || M_core~{l} == 1) // cylinder type 2
        Cylinder(ExternalCyl) = { X_winding~{l}, Y_core~{l}+0.00045 , Z_winding~{l}, 0, L_winding~{l}-((L_winding~{l}-l_core~{l})/2)-2*G_winding~{l}, 0, R_winding~{l}-G_winding~{l} };
        InternalCyl = newv;
        Cylinder(InternalCyl) = { X_winding~{l}, Y_core~{l}+0.00045 , Z_winding~{l}, 0, L_winding~{l}-((L_winding~{l}-l_core~{l})/2)-2*G_winding~{l}, 0, 1.3*r_winding~{l} };
        lLabel = newv;
        BooleanDifference(lLabel) =  {Volume{ExternalCyl}; Delete;} {Volume{InternalCyl}; Delete;};
    EndIf

    If(M_winding~{l} == 0) // cylinder type 1
        //Rotate { {0,0,1}, {X_winding~{l},0,Z_winding~{l}}, deg*Rz_winding~{l} } { Volume{ lLabel }; }
        //Rotate { {0,1,0}, {X_winding~{l},0,Z_winding~{l}}, deg*Ry_winding~{l} } { Volume{ lLabel }; }
        //Rotate { {1,0,0}, {X_winding~{l},0,Z_winding~{l}}, deg*Rx_winding~{l} } { Volume{ lLabel }; }
    ElseIf(M_winding~{l} == 1) // SWS Microsys geometry
        //Rotate { {0,0,1}, {X_winding~{l},0,Z_winding~{l}}, deg*Rz_winding~{l} } { Volume{ lLabel }; }
        //Rotate { {0,1,0}, {X_winding~{l},0,Z_winding~{l}}, deg*Ry_winding~{l} } { Volume{ lLabel }; }
        //Rotate { {1,0,0}, {X_winding~{l},0,Z_winding~{l}}, deg*Rx_winding~{l} } { Volume{ lLabel }; }
    EndIf

    If(!Flag_3Dto2D)
        Physical Volume(Sprintf("Magnet_%g",l+NumMagnets+NumCores+NumSpacers),10*(l+NumMagnets+NumCores+NumSpacers)) = { lLabel };
        skin~{l+NumMagnets+NumCores+NumSpacers}[] = CombinedBoundary{ Volume{ lLabel }; };
        Physical Surface(Sprintf("SkinMagnet_%g",l+NumMagnets+NumCores+NumSpacers),10*(l+NumMagnets+NumCores+NumSpacers)+1) = -skin~{l+NumMagnets+NumCores+NumSpacers}[]; // Core skin
        VolMagnets[] +=  { lLabel };

        // Mesh
        Characteristic Length { PointsOf{ Volume{ lLabel}; } } = MeshCharacteristicLength_winding;

    Else
        RectangleCut_Left  = newv;
        Rectangle(RectangleCut_Left) = {0, -0.05, 0, 0.1, 0.1, 0};
        lLabel_2D_Left = newv;
        BooleanIntersection(lLabel_2D_Left) = { Volume{ lLabel }; Delete;} { Surface{RectangleCut_Left}; Delete; };
        Physical Surface(Sprintf("2DCoil_%g_Left",10*(l+NumMagnets+NumCores+NumSpacers)),10*(l+NumMagnets+NumCores+NumSpacers)) = {lLabel_2D_Left};
        SurfMagnets2D[] +=  { lLabel_2D_Left };

        // Mesh
        Characteristic Length { PointsOf{ Surface{ lLabel_2D_Left}; } } = MeshCharacteristicLength_winding;

    EndIf
    
EndFor