/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     22/08/2018
% Version:  V1
%
% List of all parameters (geometry and simulation) for Gmsh interface : 
%       Input  : -
%       output : All parameters available in the Gmsh interface
****************************************************************************/
Include "Flag3Dto2D.pro";
mm = 1.e-3;
deg = Pi/180.;
DeuxPiRad = 2*Pi;

// Main parameters
DefineConstant[
  Flag_FullMenu = {0, Choices{0,1}, Name "Show all parameters"}
  //Flag_3Dto2D = {1, Choices{0,1}, Name "Parameters/1Geometry/0 2D or 3D (tick for 2D)"} // innactive
  Flag_InfiniteBox = {1, Choices{0,1}, Name "Parameters/4Simulation/0Add infinite box"}
  Flag_NL = {0, Choices{0,1}, Name "Parameters/2Physics/1B-H curve (saturation)/0Tick for NonLinear"}
  Flag_PostProcessing = {0, Choices{0,1}, Name "Parameters/5PostProcessing/0Postprocessing analysis variable"}
  NumMagnets = {2, Min 1, Max 20, Step 1, Name "Parameters/1Geometry/0Number of magnets"}
  NumCores = {1, Min 1, Max 20, Step 1, Name "Parameters/1Geometry/0Number of cores"}
  Spacers = {1, Choices{0,1}, Name "Parameters/1Geometry/0Add spacers"}
];

NumSpacers = Spacers*NumMagnets; // Each magnet has a ferromagnetic spacer below
NumWindings = NumCores; // Each magnet has a ferromagnetic spacer below


// In case of non linear BH curve (saturation)
If(Flag_NL)
  DefineConstant[
    choice_BH = {6, Choices{1,2,3,4,5,6,7,8}, Name "Parameters/2Physics/1B-H curve (saturation)/1Curve Choice",Visible (Flag_NL == 1)}
    Nb_max_iter = {30, Min 1, Max 50, Step 1, Name "Parameters/2Physics/1B-H curve (saturation)/2Number max iteration",Visible (Flag_NL == 1)}
    relaxation_factor = {1, Min 1, Max 10, Step 1, Name "Parameters/2Physics/1B-H curve (saturation)/3Relaxation Factor",Visible (Flag_NL == 1)}
    stop_criterion = {1e-5, Min 1e-6, Max 1e-4, Step 1, Name "Parameters/2Physics/1B-H curve (saturation)/4Stop Criterion",Visible (Flag_NL == 1)}
  ];
EndIf

// Mechanical constant
DefineConstant[ 
    Inertia = { 5e-3, Min -1, Max 1, Step 1e-4,
      Name "Parameters/2Physics/0Mechanics/00Inertial mass [kg]", Highlight "PaleVioletRed"},
    k_spring = { 150, Min -10000, Max 10000, Step 10,
      Name "Parameters/2Physics/0Mechanics/00Spring stifness [N m-1]", Highlight "PaleVioletRed"}, 
    b_air = { 5e-2, Min 0, Max 1e-1, Step 1e-4,
      Name "Parameters/2Physics/0Mechanics/00Air Damping [N s m-1]", Highlight "PaleVioletRed"}, 
    F_friction = { 0, Min -1000, Max 1000, Step 1,
      Name "Parameters/2Physics/0Mechanics/00friction force [N]", Highlight "PaleVioletRed"}
  ];


If(Flag_3Dto2D) // 2D
  DefineConstant[
    Flag_AnalysisType = {0,  Choices{0="Static",  1="Dynamic"},
      Name "Parameters/4Simulation/00Type of analysis", Highlight "Blue",
      Help Str["- Use 'Static' to compute static fields created by the magnets in the ferromagnetic core",
        "- Use 'Dynamic' to compute the dynamic response of the magnet"]}
  ];
If(Flag_AnalysisType==0)
  DefineConstant[ displacementY = { 0., Min -1, Max 1,
      Name "Input/2Vertical displacement", Visible 0} ];
  UndefineConstant[ "Input/1Step" ];
  UndefineConstant[ "Output/Global/2Vertical displacement" ];
EndIf
If(Flag_AnalysisType==1)
  DefineConstant[ displacementY = { 0., Min -1, Max 1,
      Name "Output/Global/2Vertical displacement", ReadOnlyRange 1} ];
  UndefineConstant[ "Input/2Vertical displacement" ];
EndIf



  DefineConstant[
  ratioBox = {1.5, Name "Parameters/4Simulation/1Infinite box/Ratio int-content", Visible Flag_FullMenu}];



  // Dynamic mode simulation
  DefineConstant[
    time_min = { 0, Visible (Flag_AnalysisType == 1),
      Name "Parameters/4Simulation/DynamicSimu/1Time min."}, 
    time_max = { 1, Min time_min, Visible (Flag_AnalysisType == 1),
      Name "Parameters/4Simulation/DynamicSimu/2Time max."},
    delta_time = { 1e-4, Min time_max/5000, Visible (Flag_AnalysisType == 1),
      Name "Parameters/4Simulation/DynamicSimu/3Time Step"},
    NbSteps = (time_max - time_min)/delta_time,
    step = {0, Min 0, Max NbSteps, Step 1, Loop  (Flag_AnalysisType == 1),
      Name "Parameters/4Simulation/DynamicSimu/4Step", Visible (Flag_AnalysisType == 1)},
    Time = {delta_time*step, Min 0, Max NbSteps, Step delta_time,
      Name "Parameters/4Simulation/DynamicSimu/6Time [s]", Highlight "Tomato", Visible (Flag_AnalysisType == 1)}, 
    Flag_Cir = 1 // for circuit
  ];
  DefineConstant[
    VV = { 0,  Name "Parameters/4Simulation/DynamicSimu/5Voltage", Highlight "AliceBlue", Visible (Flag_AnalysisType == 1)},
    velocityY = { 0., Name "Output/Global/4Vertical velocity", Visible (Flag_AnalysisType == 1)}
  ];
  DefineConstant[
    R_ = {"Analysis", Name "Parameters/4Simulation/1ResolutionChoices", Visible (Flag_3Dto2D == 1),
          Choices {"MagStaInit_a_2D", "ProjectionInit", "Analysis"}},
    C_ = {"-solve -v 3 -v2", Name "GetDP/9ComputeCommand", Visible 0},
    P_ = {"", Name "GetDP/2PostOperationChoices", Visible 0}
  ];



Else // 3D

  DefineConstant[
    // preset all getdp options and make them (in)visible
    R_ = {"MagSta_phi", Name "Parameters/4Simulation/1ResolutionChoices", Visible (Flag_3Dto2D == 0),
    Choices {"MagSta_a", "MagSta_phi"}},
    C_ = {"-solve -v 5 -v2 -bin", Name "Parameters/4Simulation/9ComputeCommand", Visible 0}
    P_ = {"", Name "Parameters/4Simulation/2PostOperationChoices", Visible 0}
  ];
  DefineConstant[
  ratioBox = {1.5, Name "Parameters/4Simulation/1Infinite box/Ratio int-content", Visible Flag_FullMenu}];
EndIf

// Parameters for each magnet (user asked)
For i In {1:NumMagnets}
  DefineConstant[
    G~{i} = {0.3*mm, Min -10*mm, Max 10*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/0G gap [m]", i) },
    L~{i} = {1.5*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1Length [m]", i)},  
    X~{i} = {0, Min -100*mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/0X position [m]", i) },
    Y~{i} = { (i-1)*(L~{i} + 1.5*G~{i}), Min -100*mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Magnet %g/0Y position [m]", i) },
    Z~{i} = {0, Min -100*mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/0Z position [m]", i) },

    M~{i} = {0, Choices{0="Type1",1="Type2"}, Highlight "Blue",
      Name Sprintf("Parameters/1Geometry/Magnet %g/00Shape", i)},

    R~{i} = {16*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1Radius [m]", i),
      Visible (M~{i} == 0) },
    r~{i} = {10*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1radius [m]", i),
      Visible (M~{i} == 0) },
    

    Lx~{i} = {50*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1X length [m]", i),
      Visible (M~{i} == 1) },
    Ly~{i} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1XY aspect ratio", i),
      Visible (M~{i} == 1) },
    Lz~{i} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/1XZ aspect ration", i),
      Visible (M~{i} == 1) },

    Rx~{i} = {0, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/2X rotation [deg]", i) },
    Ry~{i} = {0, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Magnet %g/2Y rotation [deg]", i) },
    Rz~{i} = {0, Min -Pi, Max Pi, Step Pi/180, //
      Name Sprintf("Parameters/1Geometry/Magnet %g/2Z rotation [deg]", i) },

    SIGMA~{i} = {6.66e5,
      Name Sprintf("Parameters/2Physics/3Magnet %g/3Sigma [Sm-1]", i)},
    MUR~{i} = {1.05,
      Name Sprintf("Parameters/2Physics/3Magnet %g/3Mu relative [-]", i)},
    BR~{i} = {1, 
      Name Sprintf("Parameters/2Physics/3Magnet %g/3Br [T]", i)}
  ];
EndFor



// Parameters for each core (user asked)
For j In {1:NumCores}
  DefineConstant[
    X_core~{j} = {0, Min -10*mm, Max 10*mm, Step 0.1*mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/0X position [m]", j)},
    Y_core_equilibre~{j} = {-0.6*mm, Min -4*mm, Max 4*mm, Step 0.1*mm,
      Name Sprintf("Parameters/1Geometry/Core %g/0Y position [m]", j), Highlight "ForestGreen" },
    Z_core~{j} = {0, Min -10*mm, Max 10*mm, Step 0.1*mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/0Z position [m]", j) },

    M_core~{j} = {0, Choices{0="Type1",1="Type2"}, Highlight "Blue",
      Name Sprintf("Parameters/1Geometry/Core %g/00Shape", j)},

    R_core~{j} = {6*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Core %g/1Radius [m]", j),
      Visible (M_core~{j} == 0) },
    r_core~{j} = {2*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Core %g/1radius [m]", j),
      Visible (M_core~{j} == 0) },
    L_core~{j} = {2*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Core %g/1Length [m]", j),
      Visible (M_core~{j} == 0) },
    l_core~{j} = {1*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Core %g/1length [m]", j),
      Visible (M_core~{j} == 0) },

    Lx_core~{j} = {50*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Core %g/1X length [m]", j),
      Visible (M_core~{j} == 1) },
    Ly_core~{j} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/1XY aspect ratio", j),
      Visible (M_core~{j} == 1) },
    Lz_core~{j} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/1XZ aspect ration", j),
      Visible (M_core~{j} == 1) },

    Rx_core~{j} = {0, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/2X rotation [deg]", j) },
    Ry_core~{j} = {0, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Core %g/2Y rotation [deg]", j) },
    Rz_core~{j} = {0, Min -Pi, Max Pi, Step Pi/180,
      Name Sprintf("Parameters/1Geometry/Core %g/2Z rotation [deg]", j) },

    SIGMA_core~{j} = {2e6, 
      Name Sprintf("Parameters/2Physics/2Core %g/3Sigma [Sm-1]", j)}, //sigma_lam = 10e6 *.5e-3*.5e-3/12. for laminated iron !!!!
    MUR_core~{j} = {10000.,
      Name Sprintf("Parameters/2Physics/2Core %g/3Mu relative [-]", j)},
    BR_core~{j} = {0.0,
      Name Sprintf("Parameters/2Physics/2Core %g/3Br [T]", j)},
  
    p_init = { Y_core_equilibre~{j}, Min -1, Max 1, Step 1e-4,
      Name Sprintf("Parameters/4Simulation/DynamicSimu/0Initial position"),Highlight "ForestGreen"}
  ];

  // Position of the ferromagnetic core
  If(Flag_AnalysisType==0)
    Y_core~{j} = Y_core_equilibre~{j};
    p_current = 0;
    p_0 = 0;
  Else
    p_0 = Y_core_equilibre~{j}; //defined as the equilibrium position
    p_current = p_init + displacementY; // Current position
    Y_core~{j} = p_current;
  EndIf
EndFor


// Parameters for each spacer (user asked)
For k In {1:NumSpacers}
  DefineConstant[
    X_spacer~{k} = {X~{k}, Min -100*mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/0X position [m]", k) },
    Y_spacer~{k} = {Y~{k}-(L~{k}+1.5*G~{k})/2, Min -100*mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Spacer %g/0Y position [m]", k) },
    Z_spacer~{k} = {Z~{k}, Min -100*mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/0Z position [m]", k) },
 
    M_spacer~{k} = {0, Choices{0="Type1",1="Type2"}, Highlight "Blue",
      Name Sprintf("Parameters/1Geometry/Spacer %g/00Shape", k)},

    R_spacer~{k} = {R~{k}, Min mm, Max 100*mm, Step mm, 
      Name Sprintf("Parameters/1Geometry/Spacer %g/1Radius [m]", k),
      Visible (M~{k} == 0) },
    r_spacer~{k} = {r~{k}-3*mm, Min mm, Max 100*mm, Step mm, 
      Name Sprintf("Parameters/1Geometry/Spacer %g/1radius [m]", k),
      Visible (M~{k} == 0) },
    L_spacer~{k} = {G~{k}, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Spacer %g/1Length [m]", k),
      Visible (M~{k} == 0) },

    Lx_spacer~{k} = {Lx~{k}, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Spacer %g/1X length [m]", k),
      Visible (M~{k} == 1) },
    Ly_spacer~{k} = {Ly~{k}, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/1XY aspect ratio", k),
      Visible (M~{k} == 1) },
    Lz_spacer~{k} = {Lz~{k}, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/1XZ aspect ration", k),
      Visible (M~{k} == 1) },

    Rx_spacer~{k} = {Rx~{k}, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/2X rotation [deg]", k) },
    Ry_spacer~{k} = {Ry~{k}, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Spacer %g/2Y rotation [deg]", k) },
    Rz_spacer~{k} = {Rz~{k}, Min -Pi, Max Pi, Step Pi/180,
      Name Sprintf("Parameters/1Geometry/Spacer %g/2Z rotation [deg]", k) },

    SIGMA_spacer~{k} = {2e6,
      Name Sprintf("Parameters/2Physics/4Spacer %g/3Sigma [Sm-1]", k)},
    MUR_spacer~{k} = {10000.,
      Name Sprintf("Parameters/2Physics/4Spacer %g/3Mu relative [-]", k)},
    BR_spacer~{k} = {0.0,
      Name Sprintf("Parameters/2Physics/4Spacer %g/3Br [T]", k)}
  ];
EndFor



// Parameters for the winding (user asked)
For l In {1:NumWindings}
  Y_winding~{l} = Y_core~{l};
  DefineConstant[
    G_winding~{l} = {0.1*mm, Min -10*mm, Max 10*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/0G gap [m]", l) },
    X_winding~{l} = {X_core~{l}, Min -10*mm, Max 10*mm, Step 0.1*mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/0X position [m]", l)},
    /*Y_winding~{l} = {Y_core~{l}, Min -4*mm, Max 4*mm, Step 0.1*mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/0Y position [m]", l) },*/
    Z_winding~{l} = {Z_core~{l}, Min -10*mm, Max 10*mm, Step 0.1*mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/0Z position [m]", l) },

    M_winding~{l} = {0, Choices{0="Type1",1="Type2"}, Highlight "Blue",
      Name Sprintf("Parameters/1Geometry/Winding %g/00Shape", l)},

    R_winding~{l} = {R_core~{l}-G_winding~{l}, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Winding %g/1Radius [m]", l),
      Visible (M_winding~{l} == 0) },
    r_winding~{l} = {r_core~{l}+G_winding~{l}, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Winding %g/1radius [m]", l),
      Visible (M_winding~{l} == 0) },
    L_winding~{l} = {L_core~{l}-2*G_winding~{l}, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Winding %g/1Length [m]", l),
      Visible (M_winding~{l} == 0) },

    Lx_winding~{l} = {50*mm, Min mm, Max 100*mm, Step mm,
      Name Sprintf("Parameters/1Geometry/Winding %g/1X length [m]", l), Visible Flag_FullMenu,
      Visible (M_winding~{l} == 1) },
    Ly_winding~{l} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/1XY aspect ratio", l),
      Visible (M_winding~{l} == 1) },
    Lz_winding~{l} = {50*mm, Min mm, Max 100*mm, Step mm, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/1XZ aspect ration", l),
      Visible (M_winding~{l} == 1) },

    Rx_winding~{l} = {Rx_core~{l}, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/2X rotation [deg]", l) },
    Ry_winding~{l} = {Ry_core~{l}, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/2Y rotation [deg]", l) },
    Rz_winding~{l} = {Rz_core~{l}, Min -Pi, Max Pi, Step Pi/180, Visible Flag_FullMenu,
      Name Sprintf("Parameters/1Geometry/Winding %g/2Z rotation [deg]", l) },

    SIGMA_winding~{l} = {5.96e7,
      Name Sprintf("Parameters/2Physics/5Winding %g/3Sigma [Sm-1]", l)},
    MUR_winding~{l} = {1,
      Name Sprintf("Parameters/2Physics/5Winding %g/3Mu relative [-]", l)},
    BR_winding~{l} = {0.0,
      Name Sprintf("Parameters/2Physics/5Winding %g/3Br [T]", l)},
    N_winding~{l} = {1400,
      Name Sprintf("Parameters/2Physics/5Winding %g/4Number wiring turn [-]", l)}
  ];
EndFor



//The mesh parameters for each part. 
DefineConstant[
  MeshCharacteristicLength_magnet = {0.0005, Min 0.0001, Max 0.01, Step 0.0001,
      Name Sprintf("Parameters/3Mesh/0CharacteristicLength Magnet [m]"), Highlight "PeachPuff2" },
  MeshCharacteristicLength_core = {0.0005, Min 0.0001, Max 0.01, Step 0.0001,
      Name Sprintf("Parameters/3Mesh/1CharacteristicLength Core [m]"), Highlight "PeachPuff2" },
  MeshCharacteristicLength_spacer = {0.0001, Min 0.0001, Max 0.01, Step 0.0001, //(Flag_3Dto2D==1)?0.001:0.001
      Name Sprintf("Parameters/3Mesh/2CharacteristicLength Spacer [m]"), Highlight "PeachPuff2" },
  MeshCharacteristicLength_winding = {0.001, Min 0.0001, Max 0.01, Step 0.0001,
      Name Sprintf("Parameters/3Mesh/3CharacteristicLength Winding [m]"), Highlight "PeachPuff2" },
  ratioLc = {35, Min 1, Max 1000, Step 1,
      Name Sprintf("Parameters/3Mesh/4Ratio int-Lc Infinite box"), Highlight "PeachPuff2" }
];



// The geometrical parameters of the Infinite box. 
DefineConstant[
  Flag_InfiniteBox = {1, Choices{0,1}, Name "Parameters/3Infinite box/Add infinite box"}
  Flag_TransfInf = {0, Choices{0,1}, Name "Parameters/4Simulation/3Infinite box/Transfinite mesh"}
  ratioInf = {1.5, Name "Parameters/4Simulation/3Infinite box/Ratio ext-int", Visible Flag_FullMenu}
  xInt = {1, Name "Parameters/4Simulation/3Infinite box/xInt", Visible Flag_FullMenu}
  yInt = {1, Name "Parameters/4Simulation/3Infinite box/yInt", Visible Flag_FullMenu}
  zInt = {1, Name "Parameters/4Simulation/3Infinite box/zInt", Visible Flag_FullMenu}
  xExt = {xInt*ratioInf, Name "Parameters/4Simulation/3Infinite box/xExt", Visible Flag_FullMenu}
  yExt = {yInt*ratioInf, Name "Parameters/4Simulation/3Infinite box/yExt", Visible Flag_FullMenu}
  zExt = {zInt*ratioInf, Name "Parameters/4Simulation/3Infinite box/zExt", Visible Flag_FullMenu}
  xCnt = {0, Name "Parameters/4Simulation/3Infinite box/xCenter", Visible Flag_FullMenu}
  yCnt = {0, Name "Parameters/4Simulation/3Infinite box/yCenter", Visible Flag_FullMenu}
  zCnt = {0, Name "Parameters/4Simulation/3Infinite box/zCenter", Visible Flag_FullMenu}
];



If(Flag_PostProcessing)
  DefineConstant[
    VarName     = {"" , Name "Parameters/5PostProcessing/1Analysis name (and Folder name)", Visible (Flag_PostProcessing == 1)},
    VarVal_tmp  =  {"" , Name "Parameters/5PostProcessing/2Variable name", Visible (Flag_PostProcessing == 1)}];
    VarVal  = StringToName[VarVal_tmp];
    Ext_folder  = StrCat["../Results/",VarName,"/"];
Else
    VarName     = "";
    VarVal      = 0;
    Ext_folder  = StrCat["../Results","/"];
EndIf


radiusCoreCentral = 1.3*r_winding~{1};

