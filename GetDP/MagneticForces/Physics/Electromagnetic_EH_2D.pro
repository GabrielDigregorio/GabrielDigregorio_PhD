
Include "../Parameters/Flag3Dto2D.pro";
Include "../Parameters/Electromagnetic_EH_common.pro" ;
Include "../Parameters/BH.pro";

Group {
  DomainC_Moving = Region[{}];
  DomainC_NonMoving = Region[{}];
  SkinDomainC_Moving = Region[{}];
  SkinDomainC_NonMoving = Region[{}];
  SkinSpacer = Region[{}];
  SkinCores = Region[{}];
  Vol_Layer = Region[{}];
  Vol_Force = Region[{}];

  domInfX = Region[1];
  domInfY = Region[2];
  AirBox  = Region[4];
  Outer   = Region[5];

  DomainInf = Region[{domInfX ,domInfY}];

  For i In {1:(NumMagnets)}
    MagnetRight~{i} = Region[ {(10*(i))}]; 
    SkinMagnetRight~{i} = Region[ {(10*(i)+2)} ];
    Magnet~{i} = Region[{MagnetRight~{i}}] ;
    Magnets += Region[{Magnet~{i}}] ;
    DomainC_NonMoving += Region[{Magnet~{i}}];
    SkinDomainC_NonMoving += Region[ { SkinMagnetRight~{i} } ];
  EndFor

  For j In {1:(NumCores)}
    Core~{j} = Region[ {(10*(j+NumMagnets))}]; 
    SkinCore~{j} = Region[ {(10*(j+NumMagnets)+1)} ];
    SkinCores += Region[{SkinCore~{j}}] ;
    Cores += Region[{Core~{j}}] ;
    DomainC_Moving += Region[{Core~{j}}];
    SkinDomainC_Moving += Region[{SkinCore~{j}}] ;
    LayerCore~{j} =  Region[AirBox, OnOneSideOf SkinCore~{j}] ; // force computation
    Vol_Layer += Region[LayerCore~{j}];
  EndFor
  Vol_Force = Region [ Vol_Layer ];
  
  For k In {1:(NumSpacers)}
    Spacer~{k} = Region[ {(10*(k+NumMagnets+NumCores))}]; 
    SkinSpacer~{k} = Region[ {(10*(k+NumMagnets+NumCores)+2)} ];
    SkinSpacer += Region[{SkinSpacer~{k}}];
    Spacers += Region[{Spacer~{k}}];
    DomainC_NonMoving += Region[{Spacer~{k}}];
  EndFor

  For l In {1:(NumWindings)}
    CoilR~{l} = Region[ {(10*(l+NumMagnets+NumCores+NumSpacers))}]; 
    Coil~{l} = Region[{CoilR~{l}}] ;
    Coils += Region[{Coil~{l}}];
    DomainC_Moving += Region[{Coil~{l}}];
  EndFor

  Sur_Air   = Region[{AirBox,domInfX,domInfY}] ; // SHOULD BE ADDED domInfX, domInfY
  Dummy = Region[111111]; // Dummy
  DomainS = Region[{}];
  DomainB = Region[{Coils}];
  DomainC = Region[ {Spacers, Cores} ]; // all conducting parts
  DomainCC = Region[{Sur_Air, Magnets, Coils} ];  // all non-conducting parts
  Sur_Dirichlet_a   = Region[Outer]; // add
  Dom_Hcurl_a = Region[ {Sur_Air, Magnets, Sur_Dirichlet_a} ]; // add

  DomainKin = Region[123456] ; // Dummy region number for mechanical equation

  If(!Flag_NL)
    DomainNL = Region[{}];
    DomainL  = Region[{ DomainC, DomainCC }];
  EndIf
  If(Flag_NL)
    DomainNL = Region[{ DomainC  }];
    DomainL  = Region[{ DomainCC }];
  EndIf

  Sur_Tot = Region[ {DomainL, DomainNL} ] ;
}


Function {
  DefineFunction[ js, dhdb_NL ];
  mu0 = 4*Pi*1e-7;
  mu[Sur_Air] = mu0;
  hc[Sur_Air] = Vector[0,0,0] ;
  br[Sur_Air] = Vector[0,0,0] ;

  /****************************** MAGNETS ***************************/
  For i In {1:(NumMagnets)}
    // coercive field of magnets
    DefineConstant[
      HC_magnet~{i} = {-BR~{i}/mu0,
        Name Sprintf("Parameters/Magnet %g/0Coercive magnetic field [Am^-1]", i), Visible 0}
    ];
    hc[MagnetRight~{i}] = Rotate[Vector[0, HC_magnet~{i}, 0], Rx~{i}, Ry~{i}, (deg*((i+1)*180)+Rz~{i})];
    br[Magnet~{i}] = Rotate[Vector[0, BR~{i}, 0], Rx~{i}, Ry~{i}, (deg*((i+1)*180)+Rz~{i})];
    mu[Magnet~{i}] = (mu0*MUR~{i});
    //sigma[Magnet~{i}]  = SIGMA~{j};
  EndFor
  

  /****************************** CORES ***************************/
  For j In {1:NumCores}
    // coercive field of magnets
    DefineConstant[
      HC_core~{j} = { -BR_core~{j}/mu0,
        Name Sprintf("Parameters/Core %g/0Coercive magnetic field [Am^-1]", j), Visible 0}
    ];
    hc[Core~{j}] = Rotate[Vector[0, HC_core~{j}, 0], Rx_core~{j}, Ry_core~{j}, Rz_core~{j}];
    br[Core~{j}] = Rotate[Vector[0, BR_core~{j}, 0], Rx_core~{j}, Ry_core~{j}, Rz_core~{j}];

    If(!Flag_NL)
      mu[Core~{j}]  = (mu0*MUR_core~{j});
    Else
      mu[Core~{j}] = 1.0/nu~{choice_BH}[$1];
      dhdb_NL[Core~{j}] = dhdb_NL~{choice_BH}[$1];
    EndIf
    sigma[Core~{j}]  = SIGMA_core~{j};
  EndFor

  /****************************** SPACERS ***************************/
  For k In {1:NumSpacers}
    // coercive field of magnets
    DefineConstant[
      HC_spacer~{k} = { -BR_spacer~{k}/mu0,
        Name Sprintf("Parameters/Spacer %g/0Coercive magnetic field [Am^-1]", k), Visible 0}
    ];
    hc[Spacer~{k}] = Rotate[Vector[0, HC_spacer~{k}, 0], Rx_spacer~{k}, Ry_spacer~{k}, Rz_spacer~{k}];
    br[Spacer~{k}] = Rotate[Vector[0, BR_spacer~{k}, 0], Rx_spacer~{k}, Ry_spacer~{k}, Rz_spacer~{k}];

    If(!Flag_NL)
      mu[Spacer~{k}]  = (mu0*MUR_spacer~{k}) ;
    Else
      mu[Spacer~{k}] = 1.0/nu~{choice_BH}[$1] ;
      dhdb_NL [Spacer~{k}] = dhdb_NL~{choice_BH}[$1];
    EndIf
    sigma[Spacer~{k}]  = SIGMA_spacer~{k};
  EndFor

  /****************************** COILS ***************************/
  For l In {1:NumWindings}
    // coercive field of magnets
    DefineConstant[
      HC_coil~{l} = { -BR_winding~{l}/mu0,
        Name Sprintf("Parameters/Winding %g/0Coercive magnetic field [Am^-1]", l), Visible 0}
    ];
    hc[Coil~{l}] = Rotate[Vector[0, HC_coil~{l}, 0], Rx_winding~{l}, Ry_winding~{l}, Rz_winding~{l}];
    br[Coil~{l}] = Rotate[Vector[0, BR_winding~{l}, 0], Rx_winding~{l}, Ry_winding~{l}, Rz_winding~{l}];
    mu[Coil~{l}]  = (mu0*MUR_winding~{l});
    sigma[Coil~{l}]  = SIGMA_winding~{l};
    NbWires[Coil~{l}] = N_winding~{l};
    SurfCoil[] = SurfaceArea[]{(10*(l+NumMagnets+NumCores+NumSpacers))}; // All of them have the same surface
    Idir[Region[{CoilR~{l}}]] =  1.;
  EndFor

  
  //************************** Function nu ****************************
  nu[] = 1.0/mu[]; 


  //************************** Maxwell stress tensor ****************************
  TM[] = ( SquDyadicProduct[$1] - SquNorm[$1] * TensorDiag[0.5, 0.5, 0.5] ) / mu[] ;
 

  // ******************   Force acting on the moving parts   **********************
  Friction[] = Sign[displacementY] * F_friction;
  Fspring[] = (k_spring*(p_0-p_current));
  Fmag[] = $Fmag ;       // computed in postprocessing
  Fair[] = -(b_air*velocityY) ; 
  //Fmag_vw[] = CompY[$Fmag_vw] ; // computed in postprocessing


  // ******************   Remesh and Geometry field   **********************
  time0 = time_min + step * delta_time;
  dXYZ[DomainC_NonMoving] = Vector[0., 0., 0.];
  dXYZ[DomainC_Moving] = Vector[0, DefineNumber[0, Name "DeltaU", Visible 0], 0];
  a_previousstep[] = Vector[0, 0, Field[XYZ[]-dXYZ[]]] ;

}

Jacobian {
  { Name Vol ;
    Case {
      { Region All ; Jacobian VolAxi; }
      { Region Vol_Force ; Jacobian SurAxi ; }
      {Region domInfX; Jacobian VolAxiRectShell {xInt,xExt,1,xCnt,yCnt,0};}      
      {Region domInfY; Jacobian VolAxiRectShell {yInt,yExt,2,xCnt,yCnt,0};}
    }
  }
}

Integration {
  { Name I1 ; Case { { Type Gauss ; Case {
          { GeoElement Triangle   ; NumberOfPoints  6 ; }
          { GeoElement Quadrangle ; NumberOfPoints  4 ; }
          { GeoElement Line       ; NumberOfPoints  13 ; }
        } }
    }
  }
}

//Include "ElectricCircuit.pro";

Constraint {
  { Name MVP_2D ;
    Case {
      { Region Outer ; Type Assign; Value 0. ; }//SkinSpacer
      If(time0 == 0 && Flag_AnalysisType==1)
        { Region Sur_Tot ; Type InitFromResolution ; NameOfResolution MagStaInit_a_2D ; }
      EndIf
      If(time0 != 0 && Flag_AnalysisType==1)
        { Region Sur_Tot ; Type InitFromResolution ; NameOfResolution ProjectionInit ; }
      EndIf
    }
  }
  { Name GaugeCondition_a ; Type Assign ;
    Case {
      { Region Outer ; SubRegion Outer ; Value 0. ; }
    }
  }
  { Name Core~{1} ; //Force on the core
      Case {
        { Region SkinCore~{1} ; Value 1. ; }
      }
  }
  { Name CurrentPosition ;
    Case {
      { Region DomainKin ; Type Init ; Value displacementY ; }
    }
  }
  { Name CurrentVelocity ;
    Case {
      { Region DomainKin ; Type Init ; Value velocityY ; }
    }
  }


}

FunctionSpace {
  { Name Hcurl_a_2D ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support Sur_Tot ; Entity NodesOf [ All ] ; } // MUST BE CANGED 
    }
    Constraint {
      { NameOfCoef ae1 ; EntityType NodesOf  ; NameOfConstraint MVP_2D ; }
    }
  }
  { Name Core~{1} ; Type Form0 ;
      BasisFunction {
        { Name sn ; NameOfCoef un ; Function BF_GroupOfNodes ;
          Support Sur_Air ; Entity GroupsOfNodesOf[ SkinCore~{1} ] ; }
      }
      Constraint {
        { NameOfCoef un ; EntityType GroupsOfNodesOf ; NameOfConstraint Core~{1} ; }
      }
    }

  { Name Position ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_Region ;
        Support DomainKin ; Entity DomainKin ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf  ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef U ; EntityType Region ; NameOfConstraint CurrentPosition ; }
    }
  }

  { Name Velocity ; Type Scalar ;
    BasisFunction {
      { Name sr ; NameOfCoef ir ; Function BF_Region ;
        Support DomainKin ; Entity DomainKin ; } }
    GlobalQuantity {
      { Name V ; Type AliasOf  ; NameOfCoef ir ; }
    }
    Constraint {
      { NameOfCoef V ; EntityType Region ; NameOfConstraint CurrentVelocity ; }
    }
  }

}




Formulation {

  { Name Projection ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      /*{ Name un ; Type Local ; NameOfSpace Core~{1} ; }*/
    }
    Equation {
      Galerkin { [  Dof{a}, {a} ] ;
        In Region[{DomainC}] ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ -a_previousstep[], {a} ] ;
        In Region[{DomainC}] ; Jacobian Vol ; Integration I1 ; }
      /*Galerkin { [ 0 * Dof{un} , {un} ] ;
          In Sur_Air ; Jacobian Vol ; Integration I1 ; }*/
    }
  }

  { Name MagSta_a_2D ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name un ; Type Local ; NameOfSpace Core~{1} ; }
    }
    Equation {
      Galerkin { [ (1.0/mu[{d a}]) * Dof{d a}  , {d a} ] ;
        In Sur_Tot ; Jacobian Vol ; Integration I1 ; }
      Galerkin { JacNL[ dhdb_NL[{d a}] * Dof{d a} , {d a} ]  ;
        In DomainNL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ hc[]  , {d a} ] ; 
        In Magnets ; Jacobian Vol ; Integration I1 ; }
      /*Galerkin { [ -js[], {a} ] ;
        In DomainS ; Jacobian Vol ; Integration I1 ; }*/
      Galerkin { [ 0 * Dof{un} , {un} ] ;
          In Sur_Air ; Jacobian Vol ; Integration I1 ; }
    }
  }

  
  { Name MagDyn_a_2D ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name un ; Type Local ; NameOfSpace Core~{1} ; }
      /*{ Name ir ; Type Local  ; NameOfSpace Hregion_i_Mag_2D ; }
      { Name Ub ; Type Global ; NameOfSpace Hregion_i_Mag_2D [Ub] ; }
      { Name Ib ; Type Global ; NameOfSpace Hregion_i_Mag_2D [Ib] ; }
      { Name Uz ; Type Global ; NameOfSpace Hregion_Z [Uz] ; }
      { Name Iz ; Type Global ; NameOfSpace Hregion_Z [Iz] ; }*/
    }
    Equation {
      Galerkin { [ nu[{d a}] * Dof{d a}  , {d a} ] ;
        In Sur_Tot ; Jacobian Vol ; Integration I1 ; }
      Galerkin { JacNL[ dhdb_NL[{d a}] * Dof{d a} , {d a} ]  ;
        In DomainNL ; Jacobian Vol ; Integration I1 ; }

      Galerkin { [ hc[]            , {d a} ] ;
        In Magnets ; Jacobian Vol ; Integration I1 ; } // INITIALY IN Magnets

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ;
        In DomainC ; Jacobian Vol ; Integration I1 ; }

      /*Galerkin { [ -js[], {a} ] ;
        In DomainS ; Jacobian Vol ; Integration I1 ; }

      Galerkin { [ -NbWires[]/SurfCoil[] * Dof{ir} , {a} ] ;
        In DomainB ; Jacobian Vol ; Integration I1 ; }
      Galerkin { DtDof [ DeuxPiRad * NbWires[]/SurfCoil[] * Dof{a} , {ir} ] ;
        In DomainB ; Jacobian Vol ; Integration I1 ; }

      GlobalTerm { [ Resistance[]  * Dof{Ib}, {Ib} ] ; In DomainB ;}
      GlobalTerm { [ Dof{Ub} , {Ib} ] ; In DomainB ; }

      If(Flag_Cir)
        GlobalTerm { [ Dof{Uz}        , {Iz} ] ; In Resistance_Cir ; }
        GlobalTerm { [ Resistance[{Iz}] * Dof{Iz} , {Iz} ] ; In Resistance_Cir ; }

        GlobalEquation {
          Type Network ; NameOfConstraint ElectricalCircuit ;
          { Node {Ib}; Loop {Ub}; Equation {Ub}; In DomainB ; }
          { Node {Iz}; Loop {Uz}; Equation {Uz}; In DomainZt_Cir ; }
        }
      EndIf*/
      Galerkin { [ 0 * Dof{un} , {un} ] ;
          In Sur_Air ; Jacobian Vol ; Integration I1 ; }
    }
  }

  { Name Mechanical ; Type FemEquation ;
    Quantity {
      { Name V ; Type Global ; NameOfSpace Velocity [V] ; } // velocity
      { Name U ; Type Global ; NameOfSpace Position [U] ; } // position
    }
    Equation {
      GlobalTerm { DtDof [ Inertia * Dof{V} , {V} ] ; In DomainKin ; }
      GlobalTerm { [ Friction[] * Dof{V} , {V} ] ; In DomainKin ; }
      GlobalTerm { [    -Fspring[{U}] , {V} ] ; In DomainKin ; }
      GlobalTerm { [          -Fmag[] , {V} ] ; In DomainKin ; }
      GlobalTerm { [          -Fair[] , {V} ] ; In DomainKin ; }


      GlobalTerm { DtDof [ Dof{U} , {U} ] ; In DomainKin ; }
      GlobalTerm {       [-Dof{V} , {U} ] ; In DomainKin ; }
    }
  }

}

Resolution {
  { Name ProjectionInit ;
    System {
      { Name Pr ; NameOfFormulation Projection ; DestinationSystem A; }
    }
    Operation {
      GmshRead["../Results/tmp.pos"]; Generate[Pr] ; Solve[Pr] ; TransferSolution[Pr] ;
    }
  }

  { Name MagStaInit_a_2D ;
    System {
      { Name S ; NameOfFormulation MagSta_a_2D ; DestinationSystem A; }
    }
    Operation {
      If(!Flag_NL)
        Generate[S]; Solve[S];
      EndIf
      If(Flag_NL)
        IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
          GenerateJac[S] ; SolveJac[S] ; }
      EndIf
      TransferSolution[S] ;
    }
  }

  { Name  Analysis ;
    System {
      If(Flag_AnalysisType ==0)
        { Name A ; NameOfFormulation MagSta_a_2D ; }
      EndIf
      If(Flag_AnalysisType ==1)
        { Name A ; NameOfFormulation MagDyn_a_2D ; }
        { Name M ; NameOfFormulation Mechanical ; }
      EndIf
    }
    Operation {
      If(Flag_AnalysisType ==0)
        If(!Flag_NL)
          Generate[A]; Solve[A];
        EndIf
        If(Flag_NL)
          IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
            GenerateJac[A] ; SolveJac[A] ; }
        EndIf
        SaveSolution[A];
        PostOperation[MapMag] ;
      EndIf

      If(Flag_AnalysisType ==1)
        SetTime[time0] ;
        InitSolution[A] ; SaveSolution[A] ;
        InitSolution[M] ; SaveSolution[M] ;

        TimeLoopTheta[time0, time0+delta_time, delta_time, 1.]{ // do 1 step
          If(!Flag_NL)
            Generate[A]; Solve[A];
          EndIf
          If(Flag_NL)
            IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor] {
              GenerateJac[A] ; SolveJac[A] ; }
          EndIf
          SaveSolution[A];
          PostOperation[MapMag] ;

          Generate[M] ; Solve[M] ; SaveSolution[M] ;
          PostOperation[MapMec] ;
        }
      EndIf
    }
  }

}


PostProcessing {
  
 { Name MagDyn_a_2D ; NameOfFormulation MagDyn_a_2D ;
   PostQuantity {
     { Name a ; Value { Term { [  {a} ]   ; In Sur_Tot ; Jacobian Vol ; } } }
     { Name az ; Value { Term { [  CompZ[{a}] ]   ; In Sur_Tot ; Jacobian Vol ; } } }
     { Name b  ; Value { Term { [ {d a} ] ; In Sur_Tot ; Jacobian Vol ; } } }
     /*{ Name j  ; Value { Term { [ -sigma[] * Dt[{a}] ] ; In DomainC ; Jacobian Vol ; } } }
     { Name jz  ; Value { Term { [ CompZ[-sigma[] * Dt[{a}]] ] ; In Sur_Tot ; Jacobian Vol ; } } }*/

     { Name boundary  ; Value { Term { [ 1 ] ; In Dummy ; Jacobian Vol ; } } } // Dummy quantity - for visualization

     /*{ Name Flux ; Value { Integral { [ DeuxPiRad*Idir[]*NbWires[]/SurfCoil[]* CompZ[{a}] ] ; //Is it correct
           In Coils  ; Jacobian Vol ; Integration I1 ; } } }*/
     { Name W  ; Value { Integral { [ DeuxPiRad * nu[]/2*SquNorm[{d a}] ] ;
           In Sur_Tot ; Jacobian Vol ; Integration I1 ; } } }

     { Name un ; Value { Local { [ {un} ] ; In  Vol_Force; Jacobian Vol ; } } }
     { Name Fy  ; Value { Integral { [ CompY[ - DeuxPiRad * TM[{d a}] * {d un} ] ] ;
           In Sur_Tot ; Jacobian Vol ; Integration I1 ; } } }

     /*If(Flag_Cir)
       { Name Uc ; Value {
           Term { [ {Ub} ]  ; In DomainB ; }
           Term { [ {Uz} ]  ; In DomainZt_Cir ; }}}
       { Name Ic ; Value {
           Term { [ {Ib} ]  ; In DomainB ; }
           Term { [ {Iz} ]  ; In DomainZt_Cir ; }}}
     EndIf*/
   }
 }

 { Name Mechanical ; NameOfFormulation Mechanical;
   PostQuantity {
     { Name U  ; Value { Term { [ {U} ]  ; In DomainKin ; } } } //Position
     { Name DeltaU  ; Value { Term { [ {U}-displacementY ]  ; In DomainKin ; } } } //Position change
     { Name V  ; Value { Term { [ {V} ]  ; In DomainKin ; } } } //Velocity
     { Name Fmag     ; Value { Term {  Type Global; [ Fmag[] ] ; In DomainKin ; } } }
     { Name Fair     ; Value { Term {  Type Global; [ Fair[] ] ; In DomainKin ; } } }
     /*{ Name Fmag_vw  ; Value { Term {  Type Global; [ Fmag_vw[] ] ; In DomainKin ; } } }*/
     { Name Ftot     ; Value { Term {  Type Global; [  Fair[] + Fmag[] + Fspring[{U}] ] ; In DomainKin ; } } } //INITIALLY Fmag[] + Fspring[{U}]
     { Name Fspr     ; Value { Term {  Type Global; [ Fspring[{U}] ]        ; In DomainKin ; } } }
     { Name Friction ; Value { Term {  Type Global; [ Friction[{U}] * {V} ] ; In DomainKin ; } } }
   }
 }

}

ExtGmsh     = ".pos" ;
ExtGnuplot  = ".dat" ;
Ext_folder  = "Results/";

PostOperation MapMag UsingPost MagDyn_a_2D {
  Print[ boundary,  OnElementsOf Dummy, File StrCat[Ext_folder,"bnd",ExtGmsh], Format Gmsh,
    OverrideTimeStepValue step, LastTimeStepOnly ] ;
  Print[ b, OnElementsOf Sur_Tot,  File StrCat[Ext_folder,"b", ExtGmsh], Format Gmsh,
    OverrideTimeStepValue step, LastTimeStepOnly ] ;
  /*Print[ j, OnElementsOf DomainC,  File StrCat[Ext_folder,"j", ExtGmsh], Format Gmsh,
    OverrideTimeStepValue step, LastTimeStepOnly ] ;*/
  Print[ az, OnElementsOf Sur_Tot, File StrCat[Ext_folder,"az", ExtGmsh], Format Gmsh,
    OverrideTimeStepValue step, LastTimeStepOnly] ;
  Print[ az, OnElementsOf Sur_Tot, File StrCat[Ext_folder,"tmp", ExtGmsh], Format Gmsh,
    OverrideTimeStepValue 0, LastTimeStepOnly, SendToServer "No"] ;

  Print[ Fy[Sur_Air], OnGlobal, Format Table, File > "Results/Fy_2D.dat",
    SendToServer Sprintf("Output/Core %g/Y Force Magnet [N] (2D)", 1), Color "Tomato"];

  Print[ Fy[Sur_Air], OnGlobal, Format Table, File StrCat[Ext_folder,"Fmag.dat", ExtGnuplot],
    StoreInVariable $Fmag, LastTimeStepOnly] ;
 
  /*Print[ F_vw, OnRegion NodesOf[SkinDomainC_Moving], Format RegionValue,
    File StrCat["Fvw", ExtGnuplot], StoreInVariable $Fmag_vw, LastTimeStepOnly] ;*/
 
}

PostOperation MapMec UsingPost Mechanical {
    
  Print[ U, OnRegion DomainKin, File StrCat[Ext_folder,"disp", ExtGnuplot], Format Table,
         LastTimeStepOnly, SendToServer "Output/Global/2Vertical displacement", Color "Tomato"] ;
  Print[ DeltaU, OnRegion DomainKin, File StrCat[Ext_folder,"deltadisp", ExtGnuplot], Format Table,
         LastTimeStepOnly, SendToServer "Output/Global/3DeltaU", Color "Tomato" ] ;
  Print[ V, OnRegion DomainKin, File StrCat[Ext_folder,"velocity", ExtGnuplot], Format Table,
         LastTimeStepOnly, SendToServer "Output/Global/4Vertical velocity", Color "Tomato" ] ;

  Print[ Fmag, OnRegion DomainKin, Format Table, File  > StrCat[Ext_folder,"Fmag", ExtGnuplot],
         LastTimeStepOnly,SendToServer "Output/Global/5Y Force_magnet", Color "Tomato" ] ;
  /*Print[ Fmag_vw, OnRegion DomainKin, Format Table, File  > StrCat[Ext_folder,"Fmag_vw", ExtGnuplot],
         LastTimeStepOnly,SendToServer "Output/4force_vwY"] ;*/
  Print[ Fspr, OnRegion DomainKin, Format Table, File  > StrCat[Ext_folder,"Fspr", ExtGnuplot],
         LastTimeStepOnly, SendToServer "Output/Global/6Y F_spring", Color "Tomato" ] ;
  Print[ Fair, OnRegion DomainKin, Format Table, File  > StrCat[Ext_folder,"Fair", ExtGnuplot],
         LastTimeStepOnly, SendToServer "Output/Global/7Y F_air", Color "Tomato" ] ;
  Print[ Ftot, OnRegion DomainKin, Format Table, File  > StrCat[Ext_folder,"Ftot", ExtGnuplot],
         LastTimeStepOnly,SendToServer "Output/Global/8Y F_tot", Color "Tomato" ] ;
}


