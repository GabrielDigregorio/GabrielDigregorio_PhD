/***************************************************************************
% Name:     Gabriel Digregorio
% Date:     22/08/2018
% Version:  V1
%
% Compute the Maxwell stress tensor : 
%       Input  : -
%       output : Complete geometry used in GMSH
****************************************************************************/

/* -------------------------------------------------------------------
 This .pro solves the electromagnetic field 
 and the rigid-body forces acting on a set of magnetic and ferromagnetic pieces
 of either parallelepipedic or cylindrical shape.

 Besides position and dimension, each piece is attributed 
 a (constant) magnetic permeability and/or a remanence field.
 Hereafter, the pieces are all, simply though imprecisely, referred to as "Magnet", 
 irresective of whether they are truly permanent magnets or ferromagnetic parts. 

 The model proposes two dual 3D magnetostatic formulations:
 - the magnetic vector potential formulation with spanning-tree gauging;
 - the scalar magnetic potential formulation.

 As there are no conductors, the later is rather simple. The source field "hs" is
 directly the known coercive field hc[]: 
   
   h = hs - grad phi   ,  hs = -hc.

 If the "Add infinite box" box is ticked, a transformation to infinity shell is 
 used to impose the exact zero-field boundary condition at infinity. 
 The shell is generated automatically by including "InfiniteBox.geo". 
 It can be placed rather close of the magnets without loss of accuracy.

 The preferred way to compute electromagnetic forces in GetDP
 is as an explicit by-product of the Maxwell stress tensor "TM[{b}]",
 which is a material dependent function of the magnetic induction "b" field. 
 The magnetic force acting on a rigid body in empty space can be evaluated
 as the flux of the Maxwell stress tensor through a surface "S" (surrounding the body).
 A special auxiliary function "g(S)" linked "S" is defined for each magnet, i.e.
 "g(SkinMagnet~{i}) = un~{i}".
 The resultant magnetic force acting on "Magnet~{i}" is given by the integral:

 f~{i} = Integral [ TM[{b}] * {-grad un~{i}} ] ;
 
 This approach is analogous to the computation of heat flux "q(S)" through a 
 surface "S" described in "Tutorial 5: thermal problem with contact resistances".
 
 Note that the Maxwell stress tensor is always discontinuous on material discontinuities, 
 and that magnetic forces acting on rigid bodies
 depend only on the Maxwell stress tensor in empty space, 
 and on the "b" and "h" field distribution, 
 on the external side of "SkinMagnet~{i}" 
 (side of the surface in contact with air).

 "{-grad un~{i}}" in the above formula can be regarded 
 as the normal vector to "SkinMagnet~{i}"
 in the one element thick layer "layer~{i}" of finite elements 
 around "Magnet~{i}", and "f~{i}", is thus indeed the flux of "TM[]"
 through the surface of "Magnet~{i}".
 
 The support of "{-grad un~{i}}" is limited to "layer~{i}",
 which is much smaller than "AirBox".
 To speed up the computation of forces, a special domain "Vol_Force"
 for force integrations is defined, which contains only
 the layers  "layer~{i}" of all magnets.  
 -------------------------------------------------------------------*/

Include "../Parameters/Flag3Dto2D.pro";
Include "../Parameters/Electromagnetic_EH_common.pro" ;
Include "../Parameters/BH.pro";

Group{
  // Geometrical regions (give litteral labels to geometrical region numbers)
  domInfX = Region[1];
  domInfY = Region[2];
  domInfZ = Region[3];
  AirBox  = Region[4];
  Outer   = Region[5];
  RectangleCut_PostProc  = Region[3141618];

  For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
    Magnet~{i} = Region[ {(10*i)}]; 
    SkinMagnet~{i} = Region[ {(10*i+1)} ];
    Layer~{i} =  Region[AirBox, OnOneSideOf SkinMagnet~{i}] ;
  EndFor

  // Abstract Groups (group geometrical regions into formulation relevant groups)
  Vol_Inf = Region[ {domInfX, domInfY, domInfZ} ]; 
  Vol_Air = Region[ {AirBox, Vol_Inf} ];


  DomainNL = Region[{}]; //non linear domain (saturation)
  DomainL  = Region[{}]; //linear domain (no saturation)
  Vol_Magnet = Region[{}];
  Sur_Magnet = Region[{}];
  Vol_Force = Region[{}];
  For i In {1:NumMagnets+NumCores+NumSpacers+NumWindings}
    Sur_Magnet += Region[SkinMagnet~{i}]; 
    Vol_Magnet += Region[Magnet~{i}];
    Vol_Layer += Region[Layer~{i}];
    If(i>NumMagnets && i<(NumMagnets+NumCores+1))
      DomainNL = Region[Magnet~{i}];
    Else
      DomainL  = Region[Magnet~{i}];
    EndIf
  EndFor

  Vol_Tot = Region[{Vol_Air, Vol_Magnet}];

  Sur_Dirichlet_phi = Region[Outer];
  Sur_Dirichlet_a   = Region[Outer];

  Dom_Hgrad_phi = Region[ {Vol_Air, Vol_Magnet, Sur_Dirichlet_phi} ];
  Dom_Hcurl_a = Region[ {Vol_Air, Vol_Magnet, Sur_Dirichlet_a} ];
  Vol_Force = Region [ Vol_Layer ];
  //Vol_Force = Region [ Vol_Air ];
}

Function{
  mu0 = 4*Pi*1e-7;
  //mu[Vol_Air] = mu0;
  mu[Vol_Air] = mu0;

  /****************************** MAGNETS ***************************/
  For i In {1:(NumMagnets)}
    // coercive field of magnets
    DefineConstant[
      HC~{i} = {-BR~{i}/mu0,
        Name Sprintf("Parameters/Magnet %g/0Coercive magnetic field [Am^-1]", i), Visible 0}
    ];
    hc[Magnet~{i}] = Rotate[Vector[0, HC~{i}, 0], Rx~{i}, Ry~{i}, (deg*((i)*180)+Rz~{i})];
    br[Magnet~{i}] = Rotate[Vector[0, BR~{i}, 0], Rx~{i}, Ry~{i}, (deg*((i)*180)+Rz~{i})];
    mu[Magnet~{i}] = (mu0*MUR~{i});
  EndFor

  /****************************** CORES ***************************/
  For j In {1:NumCores}
    // coercive field of magnets
    DefineConstant[
      HC~{j+NumMagnets} = {-BR_core~{j}/mu0,
        Name Sprintf("Parameters/Core %g/0Coercive magnetic field [Am^-1]", j), Visible 0}
    ];
    hc[Magnet~{j+NumMagnets}] = Rotate[Vector[0, HC~{j+NumMagnets}, 0], Rx_core~{j}, Ry_core~{j}, Rz_core~{j}];
    br[Magnet~{j+NumMagnets}] = Rotate[Vector[0, BR_core~{j}, 0], Rx_core~{j}, Ry_core~{j}, Rz_core~{j}];

    If(!Flag_NL)
      mu[Magnet~{j+NumMagnets}]  = (mu0*MUR_core~{j}) ;
    Else
      mu[Magnet~{j+NumMagnets}] = 1.0/nu~{choice_BH}[$1] ;
      dhdb_NL[Magnet~{j+NumMagnets}] = dhdb_NL~{choice_BH}[$1];
    EndIf
  EndFor

  /****************************** SPACERS ***************************/
  For k In {1:NumSpacers}
    // coercive field of magnets
    DefineConstant[
      HC~{k+NumMagnets+NumCores} = {-BR_spacer~{k}/mu0,
        Name Sprintf("Parameters/Spacer %g/0Coercive magnetic field [Am^-1]", k), Visible 0}
    ];
    hc[Magnet~{k+NumMagnets+NumCores}] = Rotate[Vector[0, HC~{k+NumMagnets+NumCores}, 0], Rx_spacer~{k}, Ry_spacer~{k}, Rz_spacer~{k}];
    br[Magnet~{k+NumMagnets+NumCores}] = Rotate[Vector[0, BR_spacer~{k}, 0], Rx_spacer~{k}, Ry_spacer~{k}, Rz_spacer~{k}];

    If(!Flag_NL)
      mu[Magnet~{k+NumMagnets+NumCores}]  = (mu0*MUR_spacer~{k}) ;
    Else
      mu[Magnet~{k+NumMagnets+NumCores}] = 1.0/nu~{choice_BH}[$1] ;
      dhdb_NL [Magnet~{k+NumMagnets+NumCores}] = dhdb_NL~{choice_BH}[$1];
    EndIf
  EndFor

  /****************************** COILS ***************************/
  For l In {1:NumWindings}
    // coercive field of magnets
    DefineConstant[
      HC~{l+NumMagnets+NumCores+NumSpacers} = {-BR_winding~{l}/mu0,
        Name Sprintf("Parameters/Winding %g/0Coercive magnetic field [Am^-1]", l), Visible 0}
    ];
    hc[Magnet~{l+NumMagnets+NumCores+NumSpacers}] = Rotate[Vector[0, HC~{j+NumMagnets+NumCores+NumSpacers}, 0], Rx_winding~{j}, Ry_winding~{j}, Rz_winding~{j}];
    br[Magnet~{l+NumMagnets+NumCores+NumSpacers}] = Rotate[Vector[0, BR_winding~{l}, 0], Rx_winding~{l}, Ry_winding~{l}, Rz_winding~{l}];
    mu[Magnet~{l+NumMagnets+NumCores+NumSpacers}]  = (mu0*MUR_winding~{l}) ;
  EndFor

  // Function nu
  nu[] = 1.0/mu[]; 

  // Maxwell stress tensor (valid for both formulations)
  TM[] = ( SquDyadicProduct[$1] - SquNorm[$1] * TensorDiag[0.5, 0.5, 0.5] ) / mu[] ;
}

Jacobian {
  { Name Vol ;
    Case {
      { Region All ; Jacobian Vol ; }
      {Region domInfX; Jacobian VolRectShell {xInt,xExt,1,xCnt,yCnt,zCnt};}      
      {Region domInfY; Jacobian VolRectShell {yInt,yExt,2,xCnt,yCnt,zCnt};}
      {Region domInfZ; Jacobian VolRectShell {zInt,zExt,3,xCnt,yCnt,zCnt};}
    }
  }
}

Integration {
  { Name Int ; 
    Case {
      { Type Gauss ;
        Case {
	  { GeoElement Triangle    ; NumberOfPoints 4 ; }
	  { GeoElement Quadrangle  ; NumberOfPoints 4 ; }
    { GeoElement Tetrahedron ; NumberOfPoints 4 ; }
	  { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
	  { GeoElement Prism       ; NumberOfPoints  6 ; } 
	}
      }
    }
  }
}

Constraint {
  { Name phi ;
    Case {
      { Region Sur_Dirichlet_phi ; Value 0. ; }
    }
  }
  { Name a ;
    Case {
      { Region Sur_Dirichlet_a ; Value 0. ; }
    }
  }
  { Name GaugeCondition_a ; Type Assign ;
    Case {
      { Region Dom_Hcurl_a ; SubRegion Sur_Dirichlet_a ; Value 0. ; }
    }
  }
  For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
    { Name Magnet~{i} ;
      Case {
        { Region SkinMagnet~{i} ; Value 1. ; }
      }
    }
  EndFor
}

FunctionSpace {
  { Name Hgrad_phi ; Type Form0 ; // magnetic scalar potential
    BasisFunction {
      { Name sn ; NameOfCoef phin ; Function BF_Node ;
        Support Dom_Hgrad_phi ; Entity NodesOf[ All ] ; }
    }
    Constraint {
      { NameOfCoef phin ; EntityType NodesOf ; NameOfConstraint phi ; }
    }
  }
  { Name Hcurl_a; Type Form1; // magnetic vector potential
    BasisFunction {
      { Name se;  NameOfCoef ae;  Function BF_Edge; 
	      Support Dom_Hcurl_a ;Entity EdgesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef ae;  EntityType EdgesOf ; NameOfConstraint a; }
      { NameOfCoef ae;  EntityType EdgesOfTreeIn ; EntitySubType StartingOn ;
        NameOfConstraint GaugeCondition_a ; }
    }
  }
  // auxiliary field on layer of elements touching each magnet, for the
  // accurate integration of the Maxwell stress tensor (using the gradient of
  // this field)
  For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
    { Name Magnet~{i} ; Type Form0 ;
      BasisFunction {
        { Name sn ; NameOfCoef un ; Function BF_GroupOfNodes ;
          Support Vol_Air ; Entity GroupsOfNodesOf[ SkinMagnet~{i} ] ; }
      }
      Constraint {
        { NameOfCoef un ; EntityType GroupsOfNodesOf ; NameOfConstraint Magnet~{i} ; }
      }
    }
  EndFor
}

Formulation {
  { Name MagSta_phi ; Type FemEquation ;
    Quantity {
      { Name phi ; Type Local ; NameOfSpace Hgrad_phi ; }
      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
        { Name un~{i} ; Type Local ; NameOfSpace Magnet~{i} ; }
      EndFor
    }
    Equation {
      Galerkin { [ - mu[{d phi}] * Dof{d phi} , {d phi} ] ;
        In Vol_Tot ; Jacobian Vol ; Integration Int ; }
      Galerkin { [ - mu[{d phi}] * hc[] , {d phi} ] ;
        In Vol_Magnet ; Jacobian Vol ; Integration Int ; }
      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)} // dummy term to define dofs for fully fixed space
        Galerkin { [ 0 * Dof{un~{i}} , {un~{i}} ] ;
          In Vol_Air ; Jacobian Vol ; Integration Int ; }
      EndFor
    }
  }
  { Name MagSta_a; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
        { Name un~{i} ; Type Local ; NameOfSpace Magnet~{i} ; }
      EndFor
    }
    Equation {
      Galerkin { [ 1.0/mu[{d a}] * Dof{d a} , {d a} ] ;
        In Vol_Tot ; Jacobian Vol ; Integration Int ; }
      /*If(Flag_NL_Newton_Raphson)
        Galerkin { JacNL [ dhdb_NL[{d a}] * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian Vol ; Integration Int ; }
      EndIf*/
      Galerkin { [ 1.0/mu[{d a}] * br[] , {d a} ] ;
        In Vol_Magnet ; Jacobian Vol ; Integration Int ; }
      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)} 
      // dummy term to define dofs for fully fixed space
        Galerkin { [ 0 * Dof{un~{i}} , {un~{i}} ] ;
          In Vol_Air ; Jacobian Vol ; Integration Int ; }
      EndFor
    }
  }
}

Resolution {
  { Name MagSta_phi ;
    System {
      { Name A ; NameOfFormulation MagSta_phi ; }
    }
    Operation {
      InitSolution[A] ;
      If(!Flag_NL)
        Generate[A] ; Solve[A] ;
      EndIf
      If(Flag_NL)
        IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
          GenerateJac[A] ; SolveJac[A] ; }
      EndIf
      SaveSolution[A] ;
      
      PostOperation[MagSta_phi] ;
    }
  }
  { Name MagSta_a ;
    System {
      { Name A ; NameOfFormulation MagSta_a ; }
    }
    Operation {
      CreateDir[ Ext_folder ];
      InitSolution[A] ;
      If(!Flag_NL)
        Generate[A] ; Solve[A] ;
      EndIf
      If(Flag_NL)
        IterativeLoop[Nb_max_iter, stop_criterion, relaxation_factor]{
          GenerateJac[A] ; SolveJac[A] ; }
      EndIf
      SaveSolution[A] ;

      PostOperation[MagSta_a] ;
    }
  }
}

PostProcessing {
  { Name MagSta_phi ; NameOfFormulation MagSta_phi ;
    Quantity {
      { Name b   ; 
	      Value { 
          Local { [ - mu[{d phi}] * {d phi} ] ; In Dom_Hgrad_phi ; Jacobian Vol ; }
	        Local { [ - mu[{d phi}] * hc[] ]    ; In Vol_Magnet    ; Jacobian Vol ; } 
        } 
      }
      { Name b_norm ;
        Value { 
          Local { [Norm[ - mu[{d phi}] * {d phi} ]]; In Dom_Hgrad_phi ; Jacobian Vol; } } }
      { Name h   ; 
	      Value { 
          Local { [ - {d phi} ] ; In Dom_Hgrad_phi ; Jacobian Vol ; } } }
      { Name hc  ; 
        Value { 
          Local { [ hc[] ]      ; In Vol_Magnet    ; Jacobian Vol ; } } }
      { Name phi ; 
        Value { 
          Local { [ {phi} ]     ; In Dom_Hgrad_phi ; Jacobian Vol ; } } }

      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
        { Name un~{i} ; Value { Local { [ {un~{i}} ] ; In Vol_Force ; Jacobian Vol ; } } }
        { Name f~{i} ; Value { Integral { [ - TM[-mu[{d phi}] * {d phi}] * {d un~{i}} ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fx~{i} ; Value { Integral { [ CompX[- TM[-mu[{d phi}] * {d phi}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fy~{i} ; Value { Integral { [ CompY[- TM[-mu[{d phi}] * {d phi}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fz~{i} ; Value { Integral { [ CompZ[- TM[-mu[{d phi}] * {d phi}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
      EndFor
    }
  }

  { Name MagSta_a ; NameOfFormulation MagSta_a ;
    PostQuantity {
      { Name b ; Value { Local { [ {d a} ]; In Dom_Hcurl_a ; Jacobian Vol; } } }
      { Name b_norm ; Value { Local { [Norm[ {d a} ]]; In Dom_Hcurl_a ; Jacobian Vol; } } }
      { Name a ; Value { Local { [ {a} ]; In Dom_Hcurl_a ; Jacobian Vol; } } }
      { Name br ; Value { Local { [ br[] ]; In Vol_Magnet ; Jacobian Vol; } } }
      For i In {1:(NumMagnets+NumCores+NumSpacers+NumWindings)}
        { Name un~{i} ; Value { Local { [ {un~{i}} ] ; In Dom_Hcurl_a ; Jacobian Vol ; } } }
        { Name f~{i} ; Value { Integral { [ - TM[{d a}] * {d un~{i}} ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fx~{i} ; Value { Integral { [ CompX[- TM[{d a}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fy~{i} ; Value { Integral { [ CompY[- TM[{d a}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
        { Name fz~{i} ; Value { Integral { [ CompZ[- TM[{d a}] * {d un~{i}} ] ] ;
              In Vol_Force ; Jacobian Vol ; Integration Int ; } } }
      EndFor
    }
  }
}


PostOperation {
  { Name MagSta_phi ; NameOfPostProcessing MagSta_phi;
    Operation {
      Print[ b,  OnElementsOf Vol_Tot,  File StrCat["Results/",Ext_folder,"b_",VarName,Sprintf["%g",VarVal] ,".dat"] ];
      //Print[ b,  OnElementsOf Vol_Tot,  File StrCat["Results/",Ext_folder,"b_",VarName,Sprintf["%g",VarVal] ,".pos"] ];
      Echo[ Str["l=PostProcessing.NbViews-1;",
		            "View[l].ArrowSizeMax = 100;",
		            "View[l].CenterGlyphs = 1;",
		            "View[l].VectorType = 1;" ] ,
        File "PostProcessing/tmp.geo", LastTimeStepOnly] ;
      Print[ b_norm, OnElementsOf Vol_Tot, File "Results/b_norm.pos" ] ;
      For i In {NumMagnets+1:(NumCores+NumMagnets)}
      //Print[ un~{i}, OnElementsOf Domain, File "un.pos"  ];
        Print[ f~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"F_",VarName,Sprintf["%g",VarVal] ,".dat"] ];
        Print[ fx~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fx_",VarName,Sprintf["%g",VarVal] ,".dat"],
          SendToServer Sprintf("Output/Core %g/X Force [N]  Magnet (3D)", i-NumMagnets), Color "Tomato"  ];
        Print[ fy~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fy_",VarName,Sprintf["%g",VarVal] ,".dat"],
          SendToServer Sprintf("Output/Core %g/Y Force [N]  Magnet (3D)", i-NumMagnets), Color "Tomato"  ];
        Print[ fz~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fz_",VarName,Sprintf["%g",VarVal] ,".dat"], 
          SendToServer Sprintf("Output/Core %g/Z Force [N]  Magnet (3D)", i-NumMagnets), Color "Tomato"  ];
      EndFor 
      
      /*Print[ b, OnSection { {-0.053,0.053,0} {0.053,0.053,0} {0.053,-0.053,0}}, Format Table, File StrCat["Results/",Ext_folder,"bCutPlane_",VarName,Sprintf["%g",VarVal],".dat"] ];
      Print[ b, OnSection { {-0.053,0.0010,0.053} {0.053,0.0010,0.053} {0.053,0.0010,-0.053}}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux1_",VarName,Sprintf["%g",VarVal],".dat"]  ];
      Print[ b, OnSection { {-0.053,-0.0002,0.053} {0.053,-0.0002,0.053} {0.053,-0.0002,-0.053}}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux2_",VarName,Sprintf["%g",VarVal],".dat"] ];*/
      
      Print[ b, OnGrid {Cos[$A], 0.0010, Sin[$A]} { 0:2*Pi:Pi/180, 0, 0 }, Format Table, File StrCat["Results/",Ext_folder,"bForFlux1_",VarName,Sprintf["%g",VarVal],".dat"]  ];
      Print[ b, OnGrid {Cos[$A], -0.0002, Sin[$A]} { 0:2*Pi:Pi/180, 0, 0 }, Format Table, File StrCat["Results/",Ext_folder,"bForFlux2_",VarName,Sprintf["%g",VarVal],".dat"] ];
    
    }
  }
  { Name MagSta_a ; NameOfPostProcessing MagSta_a ;
    Operation {
      Print[ b,  OnElementsOf Vol_Tot,  File StrCat["Results/",Ext_folder,"b_",VarName,Sprintf["%g",VarVal] ,".dat"] ];
      Print[ b,  OnElementsOf Vol_Tot,  File StrCat["Results/",Ext_folder,"b_",VarName,Sprintf["%g",VarVal] ,".pos"] ];
      Echo[ Str["l=PostProcessing.NbViews-1;",
		            "View[l].ArrowSizeMax = 100;",
		            "View[l].CenterGlyphs = 1;",
		            "View[l].VectorType = 1;" ] ,
	    File "PostProcessing/tmp.geo", LastTimeStepOnly] ;
      Print[ br,  OnElementsOf Vol_Magnet,  File "Results/br.pos" ];

      For i In {NumMagnets+1:(NumCores+NumMagnets)}
      //Print[ un~{i}, OnElementsOf Domain, File "un.pos"  ];
        Print[ f~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"F_",VarName,Sprintf["%g",VarVal] ,".dat"] ];
        Print[ fx~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fx_",VarName,Sprintf["%g",VarVal] ,".dat"],
          SendToServer Sprintf("Output/Core %g/X Force  Magnet [N] (3D)", i-NumMagnets), Color "Tomato"  ];
        Print[ fy~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fy_",VarName,Sprintf["%g",VarVal] ,".dat"],
          SendToServer Sprintf("Output/Core %g/Y Force  Magnet [N] (3D)", i-NumMagnets), Color "Tomato"  ];
        Print[ fz~{i}[Vol_Air], OnGlobal, Format Table, File > StrCat["Results/",Ext_folder,"Fz_",VarName,Sprintf["%g",VarVal] ,".dat"], 
          SendToServer Sprintf("Output/Core %g/Z Force  Magnet [N] (3D)", i-NumMagnets), Color "Tomato"  ];
      EndFor 
      
      Print[ b, OnGrid  RectangleCut_PostProc, Format Table, File StrCat["Results/",Ext_folder,"bCutPlane_",VarName,Sprintf["%g",VarVal],".dat"] ];
      /*Print[ b, OnSection { {-0.053,0.0010,0.053} {0.053,0.0010,0.053} {0.053,0.0010,-0.053}}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux1_",VarName,Sprintf["%g",VarVal],".dat"]  ];
      Print[ b, OnSection { {-0.053,-0.0002,0.053} {0.053,-0.0002,0.053} {0.053,-0.0002,-0.053}}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux2_",VarName,Sprintf["%g",VarVal],".dat"] ];*/

      Print[ b, OnGrid {$A*Cos[$B], 0.0010, $A*Sin[$B]} {0:radiusCoreCentral:(radiusCoreCentral)/10, 0:2*Pi:Pi/10, 0}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux1_",VarName,Sprintf["%g",VarVal],".dat"] ];
      Print[ b, OnGrid {$A*Cos[$B], -0.0002, $A*Sin[$B]} {0:radiusCoreCentral:(radiusCoreCentral)/10, 0:2*Pi:Pi/10, 0}, Format Table, File StrCat["Results/",Ext_folder,"bForFlux2_",VarName,Sprintf["%g",VarVal],".dat"] ];
     

      For i In {NumMagnets+1:(NumCores+NumMagnets)}
        Print[ b_norm, OnElementsOf Region[Magnet~{i}], Format Table, File "Results/b_norm_core.txt"];
      EndFor
    }
  }
}

