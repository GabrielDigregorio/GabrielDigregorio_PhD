% Postproc. the data
close all
clear all

VarName = "";%"IntContent/"
X = 0;%transpose(1.5:0.5:10); 

VarVal_tmp = num2str(X);
extention = ".dat";
factor = 1;
for i=1:length(VarVal_tmp )
    
    % Load data
    VarVal = strrep(VarVal_tmp(i,:),' ','');
    FileName = strcat('../Results/',VarName,'bCutPlane_',VarName,VarVal,extention);
    b_CutPlane = dlmread(FileName);
    FileName = strcat('../Results/',VarName,'bForFlux1_',VarName,VarVal,extention);
    b_ForFlux1 = dlmread(FileName);
    FileName = strcat('../Results/',VarName,'bForFlux2_',VarName,VarVal,extention);
    b_ForFlux2 = dlmread(FileName);
    FileName = strcat('../Results/',VarName,'F_',VarName,VarVal,extention);
    F = dlmread(FileName);
    
    
    % Treatment on data
    if(F(2)~=0);Fx(i)=F(2);else; Fx(i)=NaN; end
    if(F(2)~=0);Fy(i)=F(3);else; Fy(i)=NaN; end
    if(F(2)~=0);Fz(i)=F(4);else; Fz(i)=NaN; end
   
    b_CutPlane_X  = mean(transpose([b_CutPlane(1:factor:end,3) b_CutPlane(1:factor:end,6) b_CutPlane(1:factor:end,9)]));
    b_CutPlane_Y  = mean(transpose([b_CutPlane(1:factor:end,4) b_CutPlane(1:factor:end,7) b_CutPlane(1:factor:end,10)]));
    b_Norm_CutPlane  = log(sqrt(b_CutPlane(1:factor:end,15).^2 + b_CutPlane(1:factor:end,16).^2 + b_CutPlane(1:factor:end,17).^2));
    index_InterestedZone = find(abs(b_CutPlane_X) < 0.025 & abs(b_CutPlane_Y) < 0.025);
    b_CutPlane_X  = b_CutPlane_X(index_InterestedZone);
    b_CutPlane_Y  = b_CutPlane_Y(index_InterestedZone);
    b_Norm_CutPlane = b_Norm_CutPlane(index_InterestedZone);
    b_x_CutPlane  = b_CutPlane(index_InterestedZone,15);
    b_y_CutPlane  = b_CutPlane(index_InterestedZone,16);
    b_z_CutPlane  = b_CutPlane(index_InterestedZone,17);
    
    
    
    
    % CHANGE THE INDICES


      figure(1)
      %[X,Y] = meshgrid(b_ForFlux1(1:factor:end,3), b_ForFlux1(1:factor:end,5));
      %Z = accumarray(b_ForFlux1(1:factor:end,[5 3]), b_ForFlux1(1:factor:end,10));%sqrt(b_ForFlux1(1:factor:end,9).^2 + b_ForFlux1(1:factor:end,10).^2 + b_ForFlux1(1:factor:end,11).^2);
%       quiver3(b_ForFlux1(1:factor:end,3), b_ForFlux1(1:factor:end,5), b_ForFlux1(1:factor:end,10))
%       xlabel('$x$ [m]','interpreter', 'latex')
%       ylabel('$y$ [m]', 'interpreter', 'latex')
%       zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
%       grid on
%       axis([-5e-3 5e-3 -5e-3 5e-3 -2 2]) 
%    
      figure(i*10)
      title('$B$ on a cut plane','interpreter', 'latex')
      subplot(2,2,1);
      s = scatter(b_CutPlane_X, b_CutPlane_Y,  5,b_Norm_CutPlane);
      title('Norm of B on a cut plane')
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-2.5e-2 2.5e-2 -2.5e-2 2.5e-2]) 
      box on
      subplot(2,2,2);
      s = scatter(b_CutPlane_X, b_CutPlane_Y, 5,b_x_CutPlane);
      title('B_x on a cut plane')
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-2.5e-2 2.5e-2 -2.5e-2 2.5e-2]) 
      box on
      subplot(2,2,3);
      s = scatter(b_CutPlane_X, b_CutPlane_Y,  5,b_y_CutPlane);
      title('B_y on a cut plane')
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-2.5e-2 2.5e-2 -2.5e-2 2.5e-2]) 
      box on
      subplot(2,2,4);
      s = scatter(b_CutPlane_X, b_CutPlane_Y,  5,b_z_CutPlane);
      title('B_z on a cut plane')
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-2.5e-2 2.5e-2 -2.5e-2 2.5e-2])  
      box on
      
      figure(3)
      stem3(b_ForFlux1(1:factor:end,3), b_ForFlux1(1:factor:end,5), b_ForFlux1(1:factor:end,10))
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-5e-3 5e-3 -5e-3 5e-3 -2 2])   
     

      figure(4)
      stem3(b_ForFlux2(1:factor:end,3), b_ForFlux2(1:factor:end,5), b_ForFlux2(1:factor:end,10))
      xlabel('$x$ [m]','interpreter', 'latex')
      ylabel('$y$ [m]', 'interpreter', 'latex')
      zlabel('$\| \!R \{ \textbf{B}\} \|$[T]', 'interpreter', 'latex')
      grid on
      axis([-5e-3 5e-3 -5e-3 5e-3 -2 2])     

    % Compute the magnetic flux
    r = 0.0015;
    Surface_cut = pi*r^2;
    phi_1 = abs(sum(b_ForFlux1(1:factor:end,10))*Surface_cut)

end


% Plot the data
figure(6)
plot(X, Fx, X, Fy, X, Fz )
xlabel(strcat(VarName, '[-]'),'interpreter', 'latex')
ylabel('$F$[N]', 'interpreter', 'latex')
legend('Fx','Fy','Fz')
grid on
box on




