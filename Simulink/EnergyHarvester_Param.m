%**************************************************************************
% Name:     Gabriel Digregorio
% Date:     10/08/2018
% Version:  V1
% 
% List of parameters used in Simulink EnergyHarvester.slx
%**************************************************************************

%% Parameters

% Mechanical parameters
m   = 46e-6;                    % [kg] mass of the proof mass
k_s = 150;                      % [N/m] Stiffness of the spring
b_a = 2e-3;                     % [Ns/m] Damping coefficient of the air

% Electromagnetic parameters
k_magnet = 100;                 % [N/m] Equivalent stiffness of the magnet
b_magnet = 3e-3;                % [Ns/m] Equivalent damping of the magnet

% Capacitive parameters
epsilon_0 = 8.854e-12;          % [F/m] Vacuum permitivity
epsilon_r = 1;                  % [-] Relative permitivity of vacuum
epsilon   = epsilon_0*epsilon_r;
A_cap     = (1e-4)^2;           % [m^2] Effective Area of the 2 plates

% Generated pulse for electrostatic EH
InputPulse      = 0;            % [-] (NOT USED)
InitialPosition = 0; %(or 10e-6)% [m] Initial condition on the position x

